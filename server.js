
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const router = express.Router();
const PubNub = require('pubnub');
const cors = require('cors')

const app = express();

app.use(cors());
app.use(bodyParser.json());

const  publishKey = 'pub-c-ceca633b-027b-4fb3-b94b-6a1994ca58a8';
const subscribeKey = 'sub-c-4b3c519a-a9cf-11e9-a82a-4e2bb732e1b4';
const secretKey = 'sec-c-ODc1ZDM2N2MtZDg4YS00MjZjLTk3MWUtYjRjOTI3YTEyMzg3';
const PRESENCE_CHANNEL = 'user-presence-channel';

const pubnub = new PubNub({
   publishKey, subscribeKey, secretKey
});




router.post('/grant', async function(req, res) {
    const {channel} = req.body;
    const authKey = hex_to_ascii(channel);
    pubnub.grant(
        {
            channels: [channel],
            authKeys: [authKey],
            read: true,
            write: true,
            ttl: 0
        },
        function (status, response) {
            if (status.error) {
                res.sendStatus(status);
            } else {
                res.json({status: "granted", publishKey, subscribeKey, authKey, channel });
            }
        }
    );
});

router.post('/grantForPresence', async function(req, res) {
    const authKey =  "sample_presence_auth_key";
    pubnub.grant(
        {
            channels: [PRESENCE_CHANNEL],
            authKeys: [authKey],
            read: false,
            write: false,
            ttl: 0
        },
        function (status, response) {
            if (status.error) {
                res.sendStatus(status);
            } else {
                res.json({status: "granted", publishKey, subscribeKey, authKey, channel: PRESENCE_CHANNEL });
            }
        }
    );
});


function hex_to_ascii(str1)
 {
	var hex  = str1.toString();
	var str = '';
	for (var n = 0; n < hex.length; n += 2) {
		str += String.fromCharCode(parseInt(hex.substr(n, 2), 16));
	}
	return str;
 }

 
app.use('/', router);
console.log(path.join(__dirname+'/dist/index.html'))
app.use(express.static(__dirname + '/dist'));

app.get('/*', function(req,res) {
    res.sendFile(path.join(__dirname+'/dist/index.html'));
});

const port = process.env.PORT || 4040;
app.listen(port, () => {
    console.log('Server is listening on port ' + 4040);
});


module.exports = app;
