import { Component, OnInit } from '@angular/core';
import * as pubnub from 'pubnub';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import {helper} from "./helper";
import { Title } from '@angular/platform-browser';
import {environment} from '../../../environments/environment';

const {createUniqueChannel} = helper;
const GRANT_URL = `${environment.url}grant`;
const PRESENCE_GRANT_URL = `${environment.url}grantForPresence`;


@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {

  init: any;
  name: string;
  users: any[] = [];
  presenceChannel: string;
  publishKey: string;
  left: boolean = false;

  constructor(private route: ActivatedRoute, private http: HttpClient, private titleService: Title) { }

  async ngOnInit(){
    this.getName();
    if(this.name){
      await this.initialize();
      this.subscribe();
      this.addListners();
      this.left = false;
    }else{
      alert("name is missing, add ?name=<YOUR_NAME_HERE> in the url");
    }
  }

  // fetches user name from query params and register as new user
  getName(){
    this.name = this.route.snapshot.queryParams['name'];
    this.titleService.setTitle(this.name);
  }

   initialize(){
    return new Promise((resolve, reject)=>{
      this.http.post(PRESENCE_GRANT_URL, {}).subscribe((resp: any) => {
          const {publishKey, subscribeKey, channel, authKey} = resp;
          this.presenceChannel = channel;
          const payload = {
            publishKey,
            subscribeKey,
            authKey,
            uuid: this.name,
          }
        this.init = new pubnub(payload);
        resolve();
      }, err => reject(err));
    });
  }

  subscribe(){
    const payload = {
      channels: [this.presenceChannel],
      withPresence: true
    };
    this.init.subscribe(payload);
  }


  
  // adds event listners after init
  addListners(){
    this.init.addListener({
      status: (response) => {
        if (response.category === "PNConnectedCategory") {
          this.hereNow();
        }
      },
      presence: (response) => {
        if(response) {
          switch(response.action) {
            case 'join': {
              this.join(response);
              break;
            };
            case 'leave': {
              this.leave(response);
              break;
            };
            default : {
              break;
            }
          }
        }
      },
      message: (resp) => {
        if (resp.channel) {
          const i = this.users.findIndex(item => item.channel === resp.channel);
          if(i > -1 && this.users[i]['response']){
            const msgIndex = this.users[i]['response'].findIndex(msg => msg.entry.time === resp.message.time);
            if(msgIndex === -1){
              this.users[i].response = [
                {
                  time: resp.time,
                  timetoken: resp.timetoken, 
                  entry:resp.message
                },
                ...this.users[i].response
              ]
            }
          }
        }
      }
    });
  }

  // adds existing users to widow on inital connect
  hereNow() {
    this.init.hereNow(
      {
        channels: [this.presenceChannel],
        includeUUIDs: true,
        includeState: true
      },
        (status, response) => {
        const users = response.channels[this.presenceChannel].occupants;
        if (users.length > 0) {
          users.forEach(user => {
            const filtered = this.users.filter(item => item.name === user.uuid);
            if (this.name !== user.uuid && filtered.length === 0) {
              this.users.push({name: user.uuid, subscribed: false});
              this.startChat(this.users.length - 1);
            }
          });
        }
      });
  }

  join(response) {
      if (response.uuid !== undefined) {
          const filtered = this.users.filter(item => item.name === response.uuid);
          if (this.name !== response.uuid && filtered.length === 0) {
            this.users.push({name: response.uuid, subscribed: false});
            this.startChat(this.users.length - 1);
          }
      }
  }

  triggerLeave(){
    const channels = this.users.map(item => item.channel);
    this.init.unsubscribe({
      channels: [this.presenceChannel, ...channels]
    });
    this.users = [];  
    this.left = true;
  }


  leave(response) {
    const id = this.users.findIndex(user => user.channel === response.channel);
    if (id > -1) {
      this.users.splice(id, 1);
    }
  }

  // enables a chat window on user join or opening new window
  startChat(i: number) {
    this.users[i].subscribed = true;
      const channel = createUniqueChannel(this.users[i].name, this.name);
      this.http.post(GRANT_URL, {channel}).subscribe((resp: any) => {
        this.init.subscribe({channels: [channel], withPresence: true});
        this.users[i].channel = channel;
        this.fetchHistory(i);
      }, err => {
        console.log(err);
      })
  }

  // sends message
  send(i) {
    this.init.publish({
      message: {message: this.users[i].input, user: this.name, time: new Date().getTime()}, 
      channel: this.users[i].channel
    }, () => {
      this.users[i].input = '';
    });
  }

  // fetching history on updates
  fetchHistory(i: number) {
    this.init.history({channel: this.users[i].channel}, (status, response) => {
      response.messages.reverse();
      this.users[i].response = response.messages;
    });
  }
}
