
export const helper = {
  createUniqueChannel: (name1: string, name2: string) => {
    const arr = [name1, name2].sort();
    if (name1 === 'C') {
      arr[0] = name1;
      arr[1] = name2;
    }
    if (name2 === 'C') {
      arr[1] = name1;
      arr[0] = name2;
    }
    let codedString = `${arr[0]}.${arr[1]}`;
    return codedString;
  }
}